# Workshop 1: Introduction to Anaconda

This is the first in a series of 6 scientific Python lectures at Risø campus,
Denmark Technical University.

## Workshop objective

To introduce new Python users to the Python language and the Anaconda
distribution. This includes the Anaconda Prompt, Spyder, Jupyter, Python
consoles, and iPython consoles.

## Who should come

This workshop is geared towards anyone who would like systematic knowledge on
what tools come with Anaconda, including both script-based or the relatively
new Jupyter notebooks. No previous experience in Python is required -- in fact,
new Python users are welcomed. We assume that the majority of users are at least
familiar with Matlab and/or Mathematica.

## Date

The workshop date and location will be announced internally at DTU Risø. Please
use the contact information below for questions on the workshop contents or 
arranging a new workshop.

## Topic outline

- What is Python and why use it?
- What is Anaconda?
- Anaconda prompt
- Command-line Python
- Python vs. iPython console
- Installing new packages
- Creating and managing environments
- Spyder
- Jupyter

## Prerequisites

If you are attending the workshop, please do the following before attending:
1. If you do not have Anaconda installed, please [install it](https://www.anaconda.com/download/)
**with Python 3.6**
2. If you have Anaconda installed, please either  
    a) have your root environment be Python 3.6, or  
    b) [create an environment](https://conda.io/docs/user-guide/tasks/manage-environments.html#creating-an-environment-with-commands)
    that has Python 3.6

## Contact

Jenni Rinker  
rink@dtu.dk