# -*- coding: utf-8 -*-
"""Joe Bob writes the best functions...in Python 2.7 and NumPy 1.11
"""

import numpy as np

num = np.random.random_integers(0, high=100, size=1)[0]
print 'Your random number is...' + str(num)

